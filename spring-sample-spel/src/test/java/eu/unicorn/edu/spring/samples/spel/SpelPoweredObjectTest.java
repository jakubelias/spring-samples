package eu.unicorn.edu.spring.samples.spel;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:spring-sample-spel-context.xml")
public class SpelPoweredObjectTest {

    @Autowired
    private SpelPoweredObject spelPoweredObject;

    @Test
    public void randomNumberTest() {
    Assert.assertNotNull(spelPoweredObject.getRandomNumber());
    }

    @Test
    public void upperCaseTest() {
        Assert.assertEquals("HELLO", spelPoweredObject.getUpperCaseString());
    }

}
