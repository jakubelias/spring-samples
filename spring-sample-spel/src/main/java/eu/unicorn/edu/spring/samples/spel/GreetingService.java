package eu.unicorn.edu.spring.samples.spel;

public class GreetingService {

    private String defaultGreeting="hello";

    public String sayHello(){
        return defaultGreeting;
    }

    public String getDefaultGreeting() {
        return defaultGreeting;
    }

    public void setDefaultGreeting(String defaultGreeting) {
        this.defaultGreeting = defaultGreeting;
    }
}
