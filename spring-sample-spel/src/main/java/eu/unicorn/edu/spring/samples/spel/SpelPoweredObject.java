package eu.unicorn.edu.spring.samples.spel;

public class SpelPoweredObject {

    private double randomNumber;

    private String upperCaseString;

    public double getRandomNumber() {
        return randomNumber;
    }

    public void setRandomNumber(double randomNumber) {
        this.randomNumber = randomNumber;
    }

    public String getUpperCaseString() {
        return upperCaseString;
    }

    public void setUpperCaseString(String upperCaseString) {
        this.upperCaseString = upperCaseString;
    }

}
