package eu.unicorn.edu.spring.samples.spel;

import org.springframework.beans.factory.annotation.Value;

public class SpelInValueExample {

    /**
     * Inject value of {@link GreetingService#getDefaultGreeting()}
     */
    @Value("#{greetingService.defaultGreeting}")
    private String greeting;

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }


}
