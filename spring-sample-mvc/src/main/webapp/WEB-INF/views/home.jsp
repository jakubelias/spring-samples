<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<html>
<head>
    <title>spring-mvc-showcase</title>
    <link href="<c:url value="/resources/form.css" />" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/resources/jqueryui/1.8/themes/base/jquery.ui.core.css" />" rel="stylesheet"
          type="text/css"/>
    <link href="<c:url value="/resources/jqueryui/1.8/themes/base/jquery.ui.theme.css" />" rel="stylesheet"
          type="text/css"/>
    <link href="<c:url value="/resources/jqueryui/1.8/themes/base/jquery.ui.tabs.css" />" rel="stylesheet"
          type="text/css"/>
</head>
<body>

<p>Actual greeting is: ${greeting.text}</p>

<form:form method="POST" action="/addGreeting" modelAttribute="greeting">
    <table>
        <tr>
            <td><form:label path="text">Name</form:label></td>
            <td><form:input path="text"/></td>
        </tr>
        <tr>
            <td><input type="submit" value="Submit"/></td>
        </tr>
    </table>
</form:form>

</body>

</html>