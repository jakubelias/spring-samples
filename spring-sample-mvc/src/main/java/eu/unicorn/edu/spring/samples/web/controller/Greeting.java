package eu.unicorn.edu.spring.samples.web.controller;

public class Greeting {

	private long id;
	private String text;

	public Greeting(String text) {
		this.text = text;
	}

	public Greeting() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}