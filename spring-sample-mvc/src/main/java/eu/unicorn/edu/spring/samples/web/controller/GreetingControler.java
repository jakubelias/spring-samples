package eu.unicorn.edu.spring.samples.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import eu.unicorn.edu.spring.samples.web.service.GreetingService;

@Controller
public class GreetingControler {

	@Autowired
	private GreetingService greetingService;

	@RequestMapping("/")
	public ModelAndView welcome() {
		String greeting = greetingService.sayHello();
		return new ModelAndView("home", "greeting", new Greeting(greeting));
	}

	@RequestMapping(value = "/addGreeting", method = RequestMethod.POST)
	public ModelAndView greetingSubmit(@ModelAttribute Greeting greeting, Model model) {
		model.addAttribute("greeting", greeting);
		return new ModelAndView("home", "greeting", greeting);
	}

	// TODO implement CRUD functionality

}
