package eu.unicorn.edu.spring.samples.web.service;

public interface GreetingService {

    String sayHello();

}
