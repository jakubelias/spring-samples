package eu.unicorn.edu.spring.samples.web.service;

import org.springframework.stereotype.Component;

@Component
public class DummyGreetingService implements GreetingService {

	@Override
	public String sayHello() {
		return "hello";
	}
}
