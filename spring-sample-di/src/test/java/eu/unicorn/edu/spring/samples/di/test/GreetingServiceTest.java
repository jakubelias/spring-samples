package eu.unicorn.edu.spring.samples.di.test;

import eu.unicorn.edu.spring.samples.di.service.GreetingService;
import eu.unicorn.edu.spring.samples.di.service.RudeGreetingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Purpose of this test is to show Spring behaviour when there are multiple implementation of single component.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:spring-sample-console-test-context.xml")
public class GreetingServiceTest {

	@Autowired
	@Qualifier("politeGreetingService")
	private GreetingService politeGreetingService;

	@Autowired
	@Qualifier("rude-greeting-service")
	private GreetingService rudeGreetingService;

	/**
	 * All implementation can be autowired to list
	 */
	@Autowired
	private List<GreetingService> services;

	@Autowired
	private BeanFactory beanFactory;

	@Test
	public void testInitialization() {
		Assert.notNull(politeGreetingService);
		Assert.notNull(rudeGreetingService);
	}

	@Test
	public void listAutowireTest() {
		org.junit.Assert.assertEquals(2,services.size());
	}

	@Test
	public void politeSayHelloTest() {
		org.junit.Assert.assertEquals("Greeting service returned wrong greeting.", "Hello to Spring", politeGreetingService.sayHello());
	}

	@Test
	public void rudeSayHelloTest() {
		org.junit.Assert.assertEquals("Greeting service returned wrong greeting.", "Go to Hell", rudeGreetingService.sayHello());
	}

	/**
	 * Spring cannot decide which implementation should be returned
	 */
	@Test(expected = NoUniqueBeanDefinitionException.class)
	public void loadBeanByInterfaceTest() {
		GreetingService bean = beanFactory.getBean(GreetingService.class);
	}

	/**
	 * It is possible to load specific implementation
	 */
	@Test
	public void loadBeanByClasstest() {
		GreetingService bean = beanFactory.getBean(RudeGreetingService.class);
		Assert.notNull(bean);
		bean.sayHello();
	}
}
