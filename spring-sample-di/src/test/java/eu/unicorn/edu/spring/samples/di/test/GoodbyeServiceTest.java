package eu.unicorn.edu.spring.samples.di.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.unicorn.edu.spring.samples.di.service.GoodbyeService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:spring-sample-console-test-context.xml")
public class GoodbyeServiceTest {

    @Autowired
    private GoodbyeService goodbyeService;

    @Test
    public void goodbyeServiceTest() {
        //we expect single implementation (the Germany one)
        org.junit.Assert.assertEquals("Goodbye service returned wrong greeting.", "auf wiedersehen", goodbyeService.sayGoodbye());
    }


}
