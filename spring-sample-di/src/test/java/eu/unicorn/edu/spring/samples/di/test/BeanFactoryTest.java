package eu.unicorn.edu.spring.samples.di.test;


import eu.unicorn.edu.spring.samples.di.service.DummyGoodbyeService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:spring-sample-console-test-context.xml")
public class BeanFactoryTest {

    @Autowired
    private BeanFactory beanFactory;

    @Test
    public void getBeanByString() {
        //use default bean name
        Object germanyGoodbyeService = beanFactory.getBean("dummyGoodbyeService");
        Assert.assertTrue(germanyGoodbyeService instanceof DummyGoodbyeService);
    }

    @Test
    public void getBeanByClass() {
        DummyGoodbyeService bean = beanFactory.getBean(DummyGoodbyeService.class);
        Assert.assertNotNull(bean);
    }
    
	@Test
    public void howToAccesBeanTest() {
        //we can still create the objects by constructor, but then it is not managed by Spring
        DummyGoodbyeService service = new DummyGoodbyeService();
        Assert.assertNull(service.getGoodbyeStringDao());

        //proper way is to use spring context
        DummyGoodbyeService bean = beanFactory.getBean(DummyGoodbyeService.class);
        Assert.assertNotNull(bean.getGoodbyeStringDao());
    }

}
