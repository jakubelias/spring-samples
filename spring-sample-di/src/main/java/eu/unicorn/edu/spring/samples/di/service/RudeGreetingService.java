package eu.unicorn.edu.spring.samples.di.service;

public class RudeGreetingService implements GreetingService {

	@Override
	public String sayHello() {
		return "Go to Hell";
	}

}
