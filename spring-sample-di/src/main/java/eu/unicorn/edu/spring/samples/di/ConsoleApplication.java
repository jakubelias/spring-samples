package eu.unicorn.edu.spring.samples.di;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import eu.unicorn.edu.spring.samples.di.service.GreetingService;
import eu.unicorn.edu.spring.samples.di.service.PoliteGreetingService;
import eu.unicorn.edu.spring.samples.di.service.RudeGreetingService;

public class ConsoleApplication {

    public static void main(String[] args) {

        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-sample-console-context.xml");


        GreetingService politeGreetingService = context.getBean(PoliteGreetingService.class);
        System.out.println("Output of polite greeting service: " + politeGreetingService.sayHello());


        RudeGreetingService rudeGreetingService = context.getBean(RudeGreetingService.class);
        System.out.println("Output of rude greeting service: " + rudeGreetingService.sayHello());

    }


}
