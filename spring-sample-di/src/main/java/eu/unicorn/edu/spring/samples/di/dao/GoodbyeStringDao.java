package eu.unicorn.edu.spring.samples.di.dao;

import java.util.List;

public interface GoodbyeStringDao {

    String find();

    List<String> findAll();

    int count();

}
