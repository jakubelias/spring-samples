package eu.unicorn.edu.spring.samples.di.dao;

import java.util.Arrays;
import java.util.List;


public class InMemoryGoodbyeStringDao implements GoodbyeStringDao {

    private List<String> goodbyeStrings = Arrays.asList("auf wiedersehen", "Goodbye", "Na shledanou");

    @Override
    public String find() {
        return goodbyeStrings.get(0);
    }

    @Override
    public List<String> findAll() {
        return null;
    }

    @Override
    public int count() {
        return goodbyeStrings.size();
    }
}
