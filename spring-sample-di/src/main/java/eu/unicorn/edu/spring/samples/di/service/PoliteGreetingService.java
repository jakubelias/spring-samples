package eu.unicorn.edu.spring.samples.di.service;

import org.springframework.stereotype.Component;

@Component
public class PoliteGreetingService implements GreetingService {

    @Override
    public String sayHello() {
        return "Hello to Spring";
    }

}
