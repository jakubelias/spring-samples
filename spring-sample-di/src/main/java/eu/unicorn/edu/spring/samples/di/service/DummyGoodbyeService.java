package eu.unicorn.edu.spring.samples.di.service;

import eu.unicorn.edu.spring.samples.di.dao.GoodbyeStringDao;
import org.springframework.beans.factory.annotation.Required;

public class DummyGoodbyeService implements GoodbyeService {
    
    private GoodbyeStringDao goodbyeStringDao;

    @Override
    public String sayGoodbye() {
        return getGoodbyeStringDao().find();
    }

    public GoodbyeStringDao getGoodbyeStringDao() {
        return goodbyeStringDao;
    }

    @Required
    public void setGoodbyeStringDao(GoodbyeStringDao goodbyeStringDao) {
        this.goodbyeStringDao = goodbyeStringDao;
    }
}
