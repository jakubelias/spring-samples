package eu.unicorn.edu.spring.samples.di.service;

public interface GreetingService {

    String sayHello();

}
