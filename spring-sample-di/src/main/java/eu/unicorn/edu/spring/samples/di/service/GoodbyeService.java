package eu.unicorn.edu.spring.samples.di.service;

public interface GoodbyeService {

    String sayGoodbye();
}
