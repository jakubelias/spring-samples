package eu.unicorn.edu.spring.samples.web.test;

import eu.unicorn.edu.spring.samples.web.dao.UserDao;
import eu.unicorn.edu.spring.samples.web.service.User;
import eu.unicorn.edu.spring.samples.web.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-sample-web-context.xml")
public class UserControllerWithMocksTest {

	@Mock
	private UserDao userDao;

	@InjectMocks
	@Autowired
	private UserService userService;

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Mock Dao layer and test just service and controller
	 * 
	 * @throws Exception
	 */
	@Test
	public void findAllUsersTest() throws Exception {

		// prepare mock response
		User karel = new User("Karel", 1, true);
		User alfred = new User("Alfred", 1, true);
		List<User> expectedUsers = Arrays.asList(karel, alfred);

		// teach mock
		when(userDao.findAll()).thenReturn(expectedUsers);

		// @formatter:off
        mockMvc.perform(get("/api/service/user/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.users", hasSize(2)))
                .andExpect(jsonPath("$.users.[0].name", equalTo("Karel")))
                .andExpect(jsonPath("$.users.[1].name", equalTo("Alfred")));
        // @formatter:on
	}

}
