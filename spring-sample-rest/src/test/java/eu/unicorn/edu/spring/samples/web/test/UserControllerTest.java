package eu.unicorn.edu.spring.samples.web.test;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests of UserController.
 */

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-sample-web-context.xml")
public class UserControllerTest {

    private static final Logger LOG = LoggerFactory.getLogger(UserControllerTest.class);

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	/**
	 * Test, that get request proceed smoothly and name of user in JSON is Josef
	 * 
	 * @throws Exception
	 */
	@Test
	public void findByIdTest() throws Exception {
		// @formatter:off
       mockMvc.perform(get("/api/service/user/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo("Josef")));
       // @formatter:on
	}

	/**
	 * Test that get request to find all users proceed smoothly.
	 *
	 * @throws Exception
	 */
	@Test
	public void findAllUsersTest() throws Exception {
        //Check that both of our heroes are returned
		// @formatter:off
        mockMvc.perform(get("/api/service/user/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.users[?(@.name == '%s')]", "Josef").exists())
                .andExpect(jsonPath("$.users[?(@.name == '%s')]", "Alois").exists());
        // @formatter:on
	}

	@Test
	public void unknownUrlTest() throws Exception {
		mockMvc.perform(get("/unknown")).andExpect(status().isNotFound());
	}

    /**
     * Load user from JSON, post it to controller and verify he has been updated with get request.
     * @throws Exception
     */
    @Test
    public void addandLoadUserTest() throws Exception {
        //load user to save
        ClassPathResource jsonResource = new ClassPathResource("requests/UserRichard.json");
        String jsonString = IOUtils.toString(jsonResource.getInputStream());
        LOG.info("Trying to insert following user: {}", jsonString);

        //save him
        // @formatter:off
        mockMvc.perform(post("/api/service/user/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString)).andExpect(status().isOk());
        // @formatter:on

        //and make sure he can be loaded
        //Id of user in JSON is 10
        mockMvc.perform(get("/api/service/user/10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo("Richard")));

    }

    /**
     * Load user from JSON, post it to controller and verify he has been updated with get request.
     * @throws Exception
     */
    @Test
    public void deleteTest() throws Exception {
        //we know that user one is Josef
        // @formatter:off
        mockMvc.perform(get("/api/service/user/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo("Josef")));
        // @formatter:on

        //lets try to delete him
        mockMvc.perform(delete("/api/service/user/1"))
                .andExpect(status().isOk());


        //and load it again
        mockMvc.perform(get("/api/service/user/1"))
                .andExpect(status().isNotFound());
        // @formatter:on
    }



    /**
     * Load user from JSON, post it to controller and verify he has been updated with get request.
     * @throws Exception
     */
    @Test
    public void updateTest() throws Exception {
        //we know that user one is Josef.
        // @formatter:off
        mockMvc.perform(get("/api/service/user/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo("Josef")))
                .andExpect(jsonPath("$.active", equalTo(true)));
        // @formatter:on

        //Lets disable him
        //Load the JSON
        ClassPathResource jsonResource = new ClassPathResource("requests/DisabledJosef.json");
        String disabledJosefString = IOUtils.toString(jsonResource.getInputStream());

        //lets try to update him
        // @formatter:off
        mockMvc.perform(put("/api/service/user/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(disabledJosefString))
                .andExpect(status().isOk());
        // @formatter:on

        // @formatter:off
        mockMvc.perform(get("/api/service/user/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo("Josef")))
                .andExpect(jsonPath("$.active", equalTo(false)));
        // @formatter:on
    }










}
