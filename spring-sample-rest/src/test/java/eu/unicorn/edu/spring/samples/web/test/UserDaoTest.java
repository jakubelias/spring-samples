package eu.unicorn.edu.spring.samples.web.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import eu.unicorn.edu.spring.samples.web.dao.UserDao;
import eu.unicorn.edu.spring.samples.web.service.User;

/**
 * User Dao is configured in test context, no mocking is used in this example.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:spring-sample-testing-test-context.xml")
public class UserDaoTest {

	@Autowired
	private UserDao userDao;

	/**
	 * Test that we properly setup list of users via Spring
	 */
	@Test
	public void findAllTest() {
		Assert.assertEquals(2, userDao.findAll().size());
	}

	/**
	 * User with id=1 is Josef
	 */
	@Test
	public void findByKnownIdTest() {
		User user = userDao.findById(1);
		Assert.assertEquals("Josef", user.getName());
	}

	/**
	 * Dao should return null when User is not found
	 */
	@Test
	public void findByUnknownIdTest() {
		Assert.assertNull(userDao.findById(100));
	}

}
