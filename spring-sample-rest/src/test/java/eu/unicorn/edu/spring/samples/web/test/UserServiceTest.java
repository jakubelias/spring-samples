package eu.unicorn.edu.spring.samples.web.test;

import eu.unicorn.edu.spring.samples.web.dao.UserDao;
import eu.unicorn.edu.spring.samples.web.service.User;
import eu.unicorn.edu.spring.samples.web.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.when;

/**
 * Tests of UserService, UserDao is mocked.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:spring-sample-testing-test-context.xml")
public class UserServiceTest {

	@Mock
	private UserDao userDao;

	@InjectMocks
	@Autowired
	private UserService userService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Mock Dao and test only service layer
	 */
	@Test
	public void mockingExampleTest() {
		// teach mock
		Assert.notNull(userDao);
		when(userDao.findById(anyInt())).thenReturn(new User("pepa", 1, true));
		Assert.notNull(userService);
		User user = userService.findById(1);
		assertEquals("pepa", user.getName());
	}

	/**
	 * Mock Dao and test if selection for active users works on service layer
	 */
	@Test
	public void findActiveUsersSingleActiveTest() {
		List<User> singleActiveUser = Collections.singletonList(new User("alfred", 1, true));
		when(userDao.findAll()).thenReturn(singleActiveUser);
		List<User> result = userService.findAllActiveUsers();
		assertEquals(1, result.size());
		assertEquals("alfred", result.get(0).getName());
	}

	/**
	 * Check that exceptions are not swallowed on service layer
	 */
	@Test(expected = RuntimeException.class)
	public void exceptionPropagationTest() {
		// teach Dao to rhrow exception
		when(userDao.findAll()).thenThrow(RuntimeException.class);
		// try to find all users
		List<User> result = userService.findAllActiveUsers();
		org.junit.Assert.fail("Exception should be thrown.");
	}



}
