package eu.unicorn.edu.spring.samples.web.service;

import eu.unicorn.edu.spring.samples.web.controller.UserDto;

public class User {

    public User(String name, int id, boolean active) {
        this.name = name;
        this.id=id;
        this.active=active;
    }

    public User(UserDto user){
        this.name = user.getName();
        this.id=user.getId();
        this.active=user.isActive();

    }

    private boolean active;

    public User() {   }

    private String name;

    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

