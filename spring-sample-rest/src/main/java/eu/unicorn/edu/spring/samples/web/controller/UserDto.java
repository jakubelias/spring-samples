package eu.unicorn.edu.spring.samples.web.controller;

import eu.unicorn.edu.spring.samples.web.service.User;

/**
 * Its a good idea to separate domain objects from UI/Rest layer
 */
public class UserDto {

    public UserDto(User user){
        this.setId(user.getId());
        this.setName(user.getName());
        this.setActive(user.isActive());
    }

    public UserDto(){};

    private String name;

    private int id;

    private boolean active;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
