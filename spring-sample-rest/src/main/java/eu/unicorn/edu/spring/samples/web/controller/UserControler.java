package eu.unicorn.edu.spring.samples.web.controller;

import eu.unicorn.edu.spring.samples.web.service.User;
import eu.unicorn.edu.spring.samples.web.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserControler {

	private static final Logger LOG = LoggerFactory.getLogger(UserControler.class);

	@Autowired
	private UserService userService;

	@RequestMapping("/hello")
	public @ResponseBody String simple() {
		return "hello";
	}

	@RequestMapping(value = "/api/service/user/", method = RequestMethod.POST)
	public UserDto createEmployee(@RequestBody UserDto user) {
		LOG.info("Start create user.");
		User result = userService.addUser(new User(user));
		return new UserDto(result);
	}

	@RequestMapping(value = "/api/service/user/{id}", method = RequestMethod.PUT)
	public UserDto updateEmployee(@PathVariable int id, @RequestBody UserDto user) {
		LOG.info("Start updating user.");
		User response = userService.updateUser(new User(user));
		return new UserDto(response);
	}

	@RequestMapping(value = "/api/service/user/{id}", method = RequestMethod.GET)
	public UserDto getUser(@PathVariable int id) {
		User user = userService.findById(id);
		if (user == null) {
			throw new UserNotFoundException();
		}
		return new UserDto(user);
	}

	@RequestMapping(value = "/api/service/user/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUser(@PathVariable int id) {
		userService.removeUser(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}


	@RequestMapping(value = "/api/service/user/", method = RequestMethod.GET)
	public UserListDto getAllUsers() {
		List<User> users = userService.findAll();
		LOG.info("Found {} users",users.size());
		return new UserListDto(users);
	}


}
