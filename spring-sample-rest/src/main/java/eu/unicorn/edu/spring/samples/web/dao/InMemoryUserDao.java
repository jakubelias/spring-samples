package eu.unicorn.edu.spring.samples.web.dao;


import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import eu.unicorn.edu.spring.samples.web.service.User;

public class InMemoryUserDao implements UserDao {

	private List<User> users;

	@Override
	public User findById(int id) {

        for (User user : users) {
			if (user.getId() == id) {
				return user;
			}
		}
		return null;
	}

	@Override
	public List<User> findAll() {
		return users;
	}

	@Override
	public User addUser(User user) {
		users.add(user);
		return user;
	}

	@Override
	public void removeUser(int id) {
		Iterator<User> itr = users.iterator();
		while(itr.hasNext()){
			if ((itr.next().getId()==id)) {
				itr.remove();
			}
		}
	}

	public List<User> getUsers() {
		return users;
	}

	@Required
	public void setUsers(List<User> users) {
		this.users = users;
	}
}
