package eu.unicorn.edu.spring.samples.web.controller;

import eu.unicorn.edu.spring.samples.web.service.User;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

public class UserListDto
{

    private List<UserDto> users = new ArrayList<UserDto>();

    public UserListDto(List<User> users){
        Assert.notNull(users);
        for (User user : users) {
            this.users.add(new UserDto(user));
        }
    }

    public UserListDto(){};


    public List<UserDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserDto> users) {
        this.users = users;
    }
}
