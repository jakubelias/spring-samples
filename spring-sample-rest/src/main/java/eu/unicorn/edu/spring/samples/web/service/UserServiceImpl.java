package eu.unicorn.edu.spring.samples.web.service;

import eu.unicorn.edu.spring.samples.web.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserDao userDao;

	@Override
	public User findById(int id) {
		LOG.info("Going to find user by id: {}", id);
		return userDao.findById(id);
	}

	@Override
	public List<User> findAll() {
		LOG.info("Going to find all users");
		return userDao.findAll();
	}

	@Override
	public List<User> findAllActiveUsers() {
		List<User> all = userDao.findAll();
		List<User> result = new ArrayList<User>();

		for (User user : all) {
			if (user.isActive()) {
				result.add(user);
			}
		}

		return result;

	}

	@Override
	public User addUser(User user) {
		LOG.info("creating user: {}", user);
		return userDao.addUser(user);
	}

	@Override
	public User updateUser(User user) {
		Assert.notNull(user);
		Assert.notNull(user.getId());
		userDao.removeUser(user.getId());
		userDao.addUser(user);
		return userDao.findById(user.getId());
	}

	@Override
	public void removeUser(int id) {
		userDao.removeUser(id);
	}
}
