package eu.unicorn.edu.spring.samples.web.service;

import java.util.List;

public interface UserService {

   User findById(int id);

   List<User> findAll();

   List<User> findAllActiveUsers();

   User addUser(User user);

   User updateUser(User user);

   void removeUser(int id);


}
