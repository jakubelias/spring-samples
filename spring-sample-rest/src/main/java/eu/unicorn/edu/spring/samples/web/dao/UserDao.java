package eu.unicorn.edu.spring.samples.web.dao;

import eu.unicorn.edu.spring.samples.web.service.User;

import java.util.List;

public interface UserDao {

    User findById(int id);

    List<User> findAll();

    User addUser(User user);

    void removeUser(int id);
}
