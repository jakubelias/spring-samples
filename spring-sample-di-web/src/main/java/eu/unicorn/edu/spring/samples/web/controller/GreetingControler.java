package eu.unicorn.edu.spring.samples.web.controller;

import eu.unicorn.edu.spring.samples.web.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GreetingControler {

	@Autowired
	private GreetingService greetingService;

	@RequestMapping("/hello")
	public @ResponseBody String simple() {
		return greetingService.sayHello();
	}

}
